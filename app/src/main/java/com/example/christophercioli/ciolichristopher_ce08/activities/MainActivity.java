package com.example.christophercioli.ciolichristopher_ce08.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.christophercioli.ciolichristopher_ce08.R;
import com.example.christophercioli.ciolichristopher_ce08.fragments.ItemsWebViewFragment;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;
import com.example.christophercioli.ciolichristopher_ce08.utilities.StorageUtility;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Member Variables
    private ArrayList<Album> mAlbums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAlbums = StorageUtility.getStoredAlbums(this);

        refreshItemsWebView(mAlbums);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAlbums = StorageUtility.getStoredAlbums(this);
        refreshItemsWebView(mAlbums);
    }

    // Menu Methods
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.buttonNewAlbum) {
            Intent newAlbumIntent = new Intent(this, FormActivity.class);
            startActivity(newAlbumIntent);
        }

        return true;
    }

    // Refresh Method
    private void refreshItemsWebView(ArrayList<Album> _albums) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.itemsFrameLayout, ItemsWebViewFragment.newInstance(_albums))
                .commit();
    }
}
