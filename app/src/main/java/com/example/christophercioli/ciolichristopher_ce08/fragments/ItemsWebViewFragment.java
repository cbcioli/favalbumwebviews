// Christopher Cioli
// JAV2 - 1805
// ItemsWebViewFragment.java

package com.example.christophercioli.ciolichristopher_ce08.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.christophercioli.ciolichristopher_ce08.R;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;
import com.example.christophercioli.ciolichristopher_ce08.utilities.AlbumWebClient;
import com.example.christophercioli.ciolichristopher_ce08.interfaces.AlbumWebInterface;

import java.util.ArrayList;

public class ItemsWebViewFragment extends Fragment {

    // Constants
    private static final String ALBUM_ARG = "ALBUM_ARG";
    private static final String TAG = "ItemsWebViewFragment";

    public ItemsWebViewFragment() { }

    public static ItemsWebViewFragment newInstance(ArrayList<Album> _albums) {

        Bundle args = new Bundle();
        args.putSerializable(ALBUM_ARG, _albums);

        ItemsWebViewFragment fragment = new ItemsWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    // Fragment Lifecycle Methods
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_items_web_view, container, false);
        setUpWebView(v);

        return v;
    }

    private void setUpWebView(View _v) {
        WebView mWebView = _v.findViewById(R.id.itemsWebView);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mWebView.setWebViewClient(new AlbumWebClient(getContext()));
            mWebView.addJavascriptInterface(new AlbumWebInterface(getContext()),
                    "AlbumInterface");
            mWebView.setWebChromeClient(new WebChromeClient() {
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.d(TAG, cm.message() + " -- From line "
                            + cm.lineNumber() + " of "
                            + cm.sourceId() );
                    return true;
                }
            });

            mWebView.loadUrl("file:///android_asset/albums.html");
        }

    }
}
