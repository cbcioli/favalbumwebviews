// Christopher Cioli
// JAV2 - 1805
// FormWebViewFragment.java

package com.example.christophercioli.ciolichristopher_ce08.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.christophercioli.ciolichristopher_ce08.R;
import com.example.christophercioli.ciolichristopher_ce08.interfaces.AlbumWebInterface;
import com.example.christophercioli.ciolichristopher_ce08.utilities.AlbumWebClient;

public class FormWebViewFragment extends Fragment {

    // Constants
    private static final String TAG = "FormWebViewFragment";

    public FormWebViewFragment() { }

    public static FormWebViewFragment newInstance() {

        Bundle args = new Bundle();

        FormWebViewFragment fragment = new FormWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_form_web_view, container, false);
        setUpWebView(v);

        return v;
    }

    private void setUpWebView(View _v) {
        WebView mWebView = _v.findViewById(R.id.formWebView);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mWebView.setWebViewClient(new AlbumWebClient(getContext()));
            mWebView.addJavascriptInterface(new AlbumWebInterface(getContext(), getActivity()),
                    "AlbumInterface");
            mWebView.setWebChromeClient(new WebChromeClient() {
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.d(TAG, cm.message() + " -- From line "
                            + cm.lineNumber() + " of "
                            + cm.sourceId() );
                    return true;
                }
            });

            mWebView.loadUrl("file:///android_asset/new_album.html");
        }

    }
}
