// Christopher Cioli
// JAV2 - 1805
// StorageUtility.java

package com.example.christophercioli.ciolichristopher_ce08.utilities;

import android.content.Context;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class StorageUtility {

    // Constants
    private static final String ALBUM_STORAGE_LOCATION = "ce08albums/";

    public static void storeAlbum(Context c, Album a) {
        File protectedStorage = c.getExternalFilesDir(ALBUM_STORAGE_LOCATION);

        if (protectedStorage != null) {
            File newAlbumFile = new File(protectedStorage, a.getFileName());

            try {
                if (newAlbumFile.createNewFile()) {
                    FileOutputStream fos = new FileOutputStream(newAlbumFile);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(a);
                    oos.close();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<Album> getStoredAlbums(Context c) {
        ArrayList<Album> albums = new ArrayList<>();
        File protectedStorage = c.getExternalFilesDir(ALBUM_STORAGE_LOCATION);

        if (protectedStorage != null) {
            File[] storedAlbums = protectedStorage.listFiles();

            for (File f : storedAlbums) {
                Album a = null;
                try {
                    FileInputStream fis = new FileInputStream(f);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    a = (Album) ois.readObject();
                    ois.close();
                    fis.close();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                if (a != null) {
                    albums.add(a);
                }
            }
        }

        return albums;
    }

    public static void deleteAlbum(Context c, Album _a) {
        File protectedStorage = c.getExternalFilesDir(ALBUM_STORAGE_LOCATION);

        if (protectedStorage != null) {
            File[] storedAlbum = protectedStorage.listFiles();

            for (File f : storedAlbum) {
                if (f.getName().equals(_a.getFileName())) {
                    if (f.delete()) {
                        break;
                    }
                }
            }
        }
    }
}
