// Christopher Cioli
// JAV2 - 1805
// AlbumWebInterface.java

package com.example.christophercioli.ciolichristopher_ce08.interfaces;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.webkit.JavascriptInterface;

import com.example.christophercioli.ciolichristopher_ce08.activities.DetailsActivity;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;
import com.example.christophercioli.ciolichristopher_ce08.utilities.StorageUtility;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class AlbumWebInterface {

    // Member Variables
    private final Context mContext;
    private FragmentActivity mFragmentActivity;
    private ArrayList<Album> mAlbums;
    private Album mSelectedAlbum;

    // Constants
    public static final String ALBUM_EXTRA = "ALBUM_EXTRA";

    public AlbumWebInterface(Context _context) {
        mContext = _context;
        mAlbums = StorageUtility.getStoredAlbums(mContext);
    }

    public AlbumWebInterface(Context _context, FragmentActivity _activity) {
        mContext = _context;
        mFragmentActivity = _activity;
        mAlbums = StorageUtility.getStoredAlbums(mContext);
    }

    public AlbumWebInterface(Context _context, FragmentActivity _activity, Album _selectedAlbum) {
        mContext = _context;
        mFragmentActivity = _activity;
        mAlbums = StorageUtility.getStoredAlbums(mContext);
        mSelectedAlbum = _selectedAlbum;
    }

    @JavascriptInterface
    public void saveAlbum(String _albumTitle, String _artistName, String _trackCount) {
        int trackCount = Integer.valueOf(_trackCount);
        Album a = new Album(_albumTitle, _artistName, trackCount);
        StorageUtility.storeAlbum(mContext, a);

        mFragmentActivity.finish();
    }

    @JavascriptInterface
    public String getStoredAlbums() {
        mAlbums = StorageUtility.getStoredAlbums(mContext);
        return Album.convertAlbumCollectionToListItems(mAlbums);
    }

    @JavascriptInterface
    public void showAlbumDetail(String _albumFilename) {
        Album clickedAlbum = null;
        for (Album a : mAlbums) {
            if (a.getFileName().equals(_albumFilename)) {
                clickedAlbum = a;
                break;
            }
        }

        Intent showDetailIntent = new Intent(mContext, DetailsActivity.class);
        showDetailIntent.putExtra(ALBUM_EXTRA, clickedAlbum);
        mContext.startActivity(showDetailIntent);
    }

    @JavascriptInterface
    public int getAlbumCount() {
        return mAlbums.size();
    }

    @JavascriptInterface
    public String getSelectedAlbumTitle() {
        return mSelectedAlbum.getAlbumTitle();
    }

    @JavascriptInterface
    public String getSelectedAlbumArtist() {
        return mSelectedAlbum.getArtistName();
    }

    @JavascriptInterface
    public int getSelectedAlbumTrackCount() {
        return mSelectedAlbum.getTrackCount();
    }

    @JavascriptInterface
    public void deleteSelectedAlbum() {
        StorageUtility.deleteAlbum(mContext, mSelectedAlbum);
        mFragmentActivity.finish();
    }
}
