package com.example.christophercioli.ciolichristopher_ce08.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.christophercioli.ciolichristopher_ce08.R;
import com.example.christophercioli.ciolichristopher_ce08.fragments.DetailsWebViewFragment;
import com.example.christophercioli.ciolichristopher_ce08.interfaces.AlbumWebInterface;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Album clickedAlbum = (Album) getIntent()
                .getSerializableExtra(AlbumWebInterface.ALBUM_EXTRA);


        refreshDetailsFragment(clickedAlbum);
    }

    private void refreshDetailsFragment(Album _a) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.detailsFrameLayout, DetailsWebViewFragment.newInstance(_a)).commit();
    }
}
