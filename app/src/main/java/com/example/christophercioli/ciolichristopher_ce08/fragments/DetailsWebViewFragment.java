// Christopher Cioli
// JAV2 - 1805
// DetailsWebViewFragment

package com.example.christophercioli.ciolichristopher_ce08.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.christophercioli.ciolichristopher_ce08.R;
import com.example.christophercioli.ciolichristopher_ce08.interfaces.AlbumWebInterface;
import com.example.christophercioli.ciolichristopher_ce08.models.Album;
import com.example.christophercioli.ciolichristopher_ce08.utilities.AlbumWebClient;

public class DetailsWebViewFragment extends Fragment {

    // Constants
    private static final String TAG = "DetailsWebViewFragment";

    public DetailsWebViewFragment() { }

    public static DetailsWebViewFragment newInstance(Album _a) {

        Bundle args = new Bundle();
        args.putSerializable(AlbumWebInterface.ALBUM_EXTRA, _a);

        DetailsWebViewFragment fragment = new DetailsWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details_web_view, container, false);

        Album a = null;
        if (getArguments() != null) {
            a = (Album) getArguments().getSerializable(AlbumWebInterface.ALBUM_EXTRA);
        }

        setUpWebView(v, a);
        return v;
    }

    private void setUpWebView(View _v, Album a) {
        WebView mWebView = _v.findViewById(R.id.detailsWebView);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mWebView.setWebViewClient(new AlbumWebClient(getContext()));
            mWebView.addJavascriptInterface(new AlbumWebInterface(getContext(), getActivity(), a),
                    "AlbumInterface");
            mWebView.setWebChromeClient(new WebChromeClient() {
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.d(TAG, cm.message() + " -- From line "
                            + cm.lineNumber() + " of "
                            + cm.sourceId() );
                    return true;
                }
            });

            mWebView.loadUrl("file:///android_asset/album_detail.html");
        }

    }
}
