// Christopher Cioli
// JAV2 - 1805
// AlbumWebClient.java

package com.example.christophercioli.ciolichristopher_ce08.utilities;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressWarnings("SameReturnValue")
public class AlbumWebClient extends WebViewClient {

    // Member Variables
    private final Context mContext;

    // Constants
    private static final String ALBUM_SCHEMA = "album-schema://";

    public AlbumWebClient(Context _context) {
        mContext = _context;
    }

    // WebViewClient Methods

    // For <= API 23
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return didHandleUrl(view, url);
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Error: " + errorCode);
        builder.setMessage(description);
        builder.show();
    }

    // For >= API 24
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return didHandleUrl(view, request.getUrl().toString());
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder.setTitle("Error: " + error.getErrorCode());
            builder.setMessage(error.getDescription());
        }
        builder.show();
    }

    // Handle Passed URLs
    @SuppressWarnings("StatementWithEmptyBody")
    private boolean didHandleUrl(@SuppressWarnings("unused") WebView _view, String _url) {
        if (_url.startsWith(ALBUM_SCHEMA)) {
            // Do some more stuff here
        }

        return true;
    }
}
