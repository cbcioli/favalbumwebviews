// Christopher Cioli
// JAV2 - 1805
// Album.java

package com.example.christophercioli.ciolichristopher_ce08.models;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings({"StringConcatenationInLoop", "unused"})
public class Album implements Serializable{

    // Constants
    private static final String TAG = "ALBUM";

    // Member Variables
    private final String mAlbumTitle;
    private final String mArtistName;
    private final int mTrackCount;
    private final String mFileName;

    // Constructor
    public Album(String _albumTitle, String _artistName, int _trackCount) {
        mAlbumTitle = _albumTitle;
        mArtistName = _artistName;
        mTrackCount = _trackCount;
        mFileName = "album_" + new Date().toString();
    }

    // Getters
    public String getAlbumTitle() {
        return mAlbumTitle;
    }

    public String getArtistName() {
        return mArtistName;
    }

    public int getTrackCount() {
        return mTrackCount;
    }

    public String getFileName() {
        return mFileName;
    }

    // Converters
    private static String convertAlbumToJSON(Album _a) {
        String outResult = "{";
        outResult += "\"title\" : \"" + _a.getAlbumTitle() + "\",";
        outResult += "\"artist\" : \"" + _a.getArtistName() + "\",";
        outResult += "\"trackCount\" : " + _a.getTrackCount() + ",";
        outResult += "\"fileName\" : \"" + _a.getFileName() + "\"";
        outResult += "}";

        return outResult;
    }

    public static String convertAlbumCollectionToListItems(ArrayList<Album> _albums) {
        String outString = "";
        for (Album a : _albums) {
            outString += "<li id=\"" + a.getFileName() + "\" onClick=\"viewAlbumDetail(this.id)\"><a href=\"\">" + a.getAlbumTitle() + " by "
                + a.getArtistName() + "</a></li>";
        }

        Log.i(TAG, outString);
        return outString;
    }

    public static String convertAlbumCollectionToJSON(ArrayList<Album> _albums) {
        String outString = "'[";
        for (int i = 0; i < _albums.size(); i++) {
            outString += convertAlbumToJSON(_albums.get(i));
            if (i != _albums.size() - 1) {
                outString += ",";
            }
        }
        outString += "]'";

        Log.i(TAG, "convertAlbumCollectionToJSON: " + outString);

        return outString;
    }
}
