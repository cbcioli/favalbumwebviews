# FavAlbumWebViews  
### by Christopher Cioli  

FavAlbumWebViews is an application that allows a user to save their favorite albums to a list.  
The application is done entirely with WebViewFragments and therefore requires HTML and Javascript to function properly. Architecturally, the WebViews receive and send data via the AlbumWebInterface which is a Java class whose methods are annotated as JavascriptInterface.  
The point of this exercise was to work with WebViews in Android and to understand how it might be useful to utilize a WebView to show web content and to control the routing of URLs manually.
